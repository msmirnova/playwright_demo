FROM mcr.microsoft.com/playwright:latest
COPY . /usr/local/bin/
WORKDIR /usr/local/bin/
RUN npm install
RUN npx playwright install --with-deps
RUN npm install -D @playwright/test