import { expect, Locator, Page } from '@playwright/test';

export class Login {
  readonly page: Page;
  readonly email_input: Locator;
  readonly password_input: Locator;
  readonly login_button: Locator;
  readonly login_error_message: Locator;

  constructor(page: Page) {
    this.page = page;
    this.email_input = page.getByLabel('Email');
    this.password_input = page.getByLabel('Password');
    this.login_button = page.getByRole('button', {name: 'Log in'}).first();
    this.login_error_message = page.getByText('Cannot login');
    }

      async open() {
        await this.page.goto('https://backoffice.tabby.dev/auth');
      }
    
      async login(email: string, password: string) {
        await this.email_input.fill(email);
        await this.password_input.fill(password);
      }
    
      async click_login_button() {
        await this.login_button.click();
      }
    
      async mock_login_request(status) {
        await this.page.route('**/auth/login', route => route.fulfill({
            status: status
          }));
      }
    
      async chek_error_login_message(){
        await expect(this.login_error_message).toBeVisible()
      }
}