import { test, expect } from '@playwright/test';
import { Login } from '../page_object/backoffice/login_page';

test('Tabby backoffice unsuccess login', async ({ page }) => {
    const loginpage = new Login(page)
    await loginpage.mock_login_request(400);
    await loginpage.open();
    await loginpage.login('example@example.com', '12345678');
    await loginpage.click_login_button();
    await page.screenshot({ path: 'screenshots/fail_login.png' });
    await loginpage.chek_error_login_message()
  })
  