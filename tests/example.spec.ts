import { test, expect } from '@playwright/test';
import { Login } from '../page_object/backoffice/login_page';

test('Tabby backoffice login', async ({ page }) => {
  const loginpage = new Login(page)
  await loginpage.open();
  await loginpage.login('example@example.com', '12345678');
  await loginpage.click_login_button();
})
